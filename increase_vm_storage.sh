#!/bin/sh

echo "RESIZE started: `date`"
echo "_______________"

echo "Clone source to VDI"
echo "____________________"
VBoxManage clonemedium "Nginx-disk001.vmdk" "cloned.vdi" --format vdi
echo "Cloning successful"
echo ""

echo "Resize the cloned VDI to 100GB"
echo "_______________________________"
VBoxManage modifymedium "cloned.vdi" --resize 102400
echo "Resize completed"
echo ""

echo "Removing old VMDK"
echo "__________________"
rm Nginx-disk001.vmdk
echo "Removal completed"
echo ""

echo "convert clone to VMDK"
echo "______________________"
VBoxManage clonemedium "cloned.vdi" "Nginx-disk001.vmdk" --format vmdk
echo "VMDK created"
echo ""

echo "RESIZE completed: `date`"

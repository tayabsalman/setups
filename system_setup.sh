#!/bin/sh

#install ssh server
sudo apt install -y openssh-server
#Allow root user acces to system
sudo echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
sudo service  ssh restart
sudo apt install -y nginx
sudo apt install -y mysql-server
sudo apt install -y redis-server
sudo apt install -y mongodb-server
sudo apt install -y php-fpm
sudo apt install -y php-redis
sudo apt install -y php-mysql


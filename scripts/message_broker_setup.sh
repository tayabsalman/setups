#!/bin/bash
cd /
apt-get update
apt-get install rabbitmq-server
service rabbitmq-server restart
#if back-end rquired install redis as back-end
apt install redis-server
service redis-server restart
apt install python3-pip
apt install nginx
pip3 install virtualenv
virtualenv drogonvenv
source drogonvenv/bin/activate
pip3 install celery==4.3.0
pip3 install flower==0.9.3
pip3 install PyMySQL==0.9.3
pip3 install pymongo==3.9.0
pip3 install pandas==0.24.2
pip3 install Django==2.2.3
pip3 install gunicorn==19.9.0
deactivate

echo "[Unit]
Description=Celery Service
After=network.target

[Service]
Type=forking
User=root
Group=root
EnvironmentFile=/etc/default/celeryd
WorkingDirectory=/System/drogon/djangoapp
ExecStart=/bin/sh -c '\${CELERY_BIN} multi start \${CELERYD_NODES} \
  -A \${CELERY_APP} --pidfile=\${CELERYD_PID_FILE} \
  --logfile=\${CELERYD_LOG_FILE} --loglevel=\${CELERYD_LOG_LEVEL} \${CELERYD_OPTS}'
ExecStop=/bin/sh -c '\${CELERY_BIN} multi stopwait \${CELERYD_NODES} \
  --pidfile=\${CELERYD_PID_FILE}'
ExecReload=/bin/sh -c '\${CELERY_BIN} multi restart \${CELERYD_NODES} \
  -A \${CELERY_APP} --pidfile=\${CELERYD_PID_FILE} \
  --logfile=\${CELERYD_LOG_FILE} --loglevel=\${CELERYD_LOG_LEVEL} \${CELERYD_OPTS}'

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/celeryd.service


echo "[Unit]
Description=Flower Celery Service

[Service]
User=root
Group=root
WorkingDirectory=/System/drogon/djangoapp
ExecStart=/drogonvenv/bin/flower --port=5555
Restart=on-failure
Type=simple

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/flower.service


echo "[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=root
Group=root
WorkingDirectory=/System/drogon/djangoapp
ExecStart=/drogonvenv/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn.sock \
          djangoapp.wsgi:application

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/gunicorn.service



echo "[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target" >> /etc/systemd/system/gunicorn.socket



echo "
# The names of the workers. This example create two workers
CELERYD_NODES="worker1 worker2"

# The name of the Celery App, should be the same as the python file
# where the Celery tasks are defined
CELERY_APP="djangoapp"

# Log and PID directories
CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
CELERYD_PID_FILE="/var/run/celery/%n.pid"

# Log level
CELERYD_LOG_LEVEL=INFO

# Path to celery binary, that is in your virtual environment
CELERY_BIN=/drogonvenv/bin/celery" >> /etc/default/celeryd


mkdir /var/log/celery /var/run/celery
chown root:root /var/log/celery /var/run/celery


systemctl daemon-reload
systemctl enable celeryd
systemctl start celeryd



systemctl daemon-reload
systemctl start flower


curl --unix-socket /run/gunicorn.sock localhost

systemctl daemon-reload
systemctl restart gunicorn



public_ip=`curl ifconfig.co`
echo "server {
    listen 80;
    server_name $public_ip;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /drogon/djangoapp;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}" >> /etc/nginx/sites-available/djangoapp

ln -s /etc/nginx/sites-available/djangoapp /etc/nginx/sites-enabled
nginx -t
systemctl restart nginx
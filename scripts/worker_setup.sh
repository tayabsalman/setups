#!/bin/bash
cd /
apt-get update
apt-get install sshpass
#if back-end rquired install redis as back-end
apt install redis-server
service redis-server restart
apt install python3-pip
pip3 install virtualenv
virtualenv drogonvenv
source drogonvenv/bin/activate
pip3 install celery==4.3.0
pip3 install PyMySQL==0.9.3
pip3 install pymongo==3.9.0
pip3 install pandas==0.24.2

echo "[Unit]
Description=Celery Service
After=network.target

[Service]
Type=forking
User=root
Group=root
EnvironmentFile=/etc/default/celeryd
WorkingDirectory=/System/drogon
ExecStart=/bin/sh -c '\${CELERY_BIN} multi start \${CELERYD_NODES} \
  -A \${CELERY_APP} --pidfile=\${CELERYD_PID_FILE} \
  --logfile=\${CELERYD_LOG_FILE} --loglevel=\${CELERYD_LOG_LEVEL} \${CELERYD_OPTS}'
ExecStop=/bin/sh -c '\${CELERY_BIN} multi stopwait \${CELERYD_NODES} \
  --pidfile=\${CELERYD_PID_FILE}'
ExecReload=/bin/sh -c '\${CELERY_BIN} multi restart \${CELERYD_NODES} \
  -A \${CELERY_APP} --pidfile=\${CELERYD_PID_FILE} \
  --logfile=\${CELERYD_LOG_FILE} --loglevel=\${CELERYD_LOG_LEVEL} \${CELERYD_OPTS}'

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/celeryd.service

echo "
# The names of the workers. This example create two workers
CELERYD_NODES="worker1 worker2"

# The name of the Celery App, should be the same as the python file
# where the Celery tasks are defined
CELERY_APP="rhaegal"

CELERYD_OPTS = "-Q rhaegal"

# Log and PID directories
CELERYD_LOG_FILE="/var/log/celery/l_%n.log"
CELERYD_PID_FILE="/var/run/celery/t_%n.pid"

# Log level
CELERYD_LOG_LEVEL=INFO

# Path to celery binary, that is in your virtual environment
CELERY_BIN=/drogonvenv/bin/celery" >> /etc/default/celeryd


mkdir /var/log/celery /var/run/celery
chown root:root /var/log/celery /var/run/celery


systemctl daemon-reload
systemctl enable celeryd
systemctl start celeryd
#!/bin/bash

sudo apt update

sudo apt -y install ssh
sudo apt -y install openssh-server

sed -i '/PasswordAuthentication/d' /etc/ssh/sshd_config
sed -i '/PermitRootLogin/d' /etc/ssh/sshd_config

echo ''
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config



Installing PHP 7.0 and dependencies
echo ''
echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
echo ''
echo "deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list

sudo wget https://www.dotdeb.org/dotdeb.gpg --no-check-certificate
sudo apt-key add dotdeb.gpg
sudo apt-get update
sudo apt-get install -y php7.0
sudo apt-get -y install php7.0-bcmath php7.0-bz2 php7.0-cli php7.0-common php7.0-curl php7.0-dba php7.0-dev php7.0-fpm php7.0-gd php7.0-imap php7.0-intl php7.0-json php7.0-ldap php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-pgsql php7.0-phpdbg php7.0-pspell php7.0-readline php7.0-recode php7.0-snmp php7.0-soap php7.0-tidy php7.0-xml php7.0-zip


sudo apt-add-repository ppa:ondrej/php
sudo apt-get update
sudo apt -y install php7.0
sudo apt -y install php7.0-bcmath php7.0-bz2 php7.0-cli php7.0-common php7.0-curl php7.0-dba php7.0-dev php7.0-fpm php7.0-gd php7.0-imap php7.0-intl php7.0-json php7.0-ldap php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-pgsql php7.0-phpdbg php7.0-pspell php7.0-readline php7.0-recode php7.0-snmp php7.0-soap php7.0-tidy php7.0-xml php7.0-zip

#sudo apt -y install php7.2-cli php7.2-common php7.2-dev php7.2-json php7.2-opcache php7.2-readline php-common php-dev php-gettext php-igbinary php-mongodb php-pear php-redis php5-cli php5-common php5-curl php5-fpm php5-gd php5-json php5-mcrypt php5-mysql php5-readline

# Installing Mysql 5.6
sudo add-apt-repository 'deb http://archive.ubuntu.com/ubuntu trusty universe'
sudo apt-get update
sudo apt -y install mysql-server-5.6 mysql-client-5.6

# Installing Python
sudo apt -y install python
sudo apt -y install python3
sudo apt -y install python-pip
sudo apt -y install python3-pip



#sudo add-apt-repository ppa:nginx/stable -y 
sudo apt -y install nginx
sudo apt-get -f install



sudo add-apt-repository ppa:chris-lea/redis-server -y 
	sudo apt-get install redis-server=2:2.8.4-1chl1~precise1
	sudo apt-get install redis-server=2:2.8.4-1chl1~raring1

# Installing Redis-Server2.8.4

sudo apt-get update
sudo apt-get
sudo apt-get install tcl8.5
wget http://download.redis.io/releases/redis-2.8.4.tar.gz
tar xzf redis-2.8.4.tar.gz
cd redis-2.8.4
make
make test
sudo make install
cd utils
sudo ./install_server.sh
#sudo service redis_6379 start
#sudo update-rc.d redis_6379 defaults


#Installing Mongodb3.6.2

echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt update
sudo apt-get install mongodb-org=3.6.2 mongodb-org-server=3.6.2 mongodb-org-shell=3.6.2 mongodb-org-mongos=3.6.2 mongodb-org-tools=3.6.2


sed -i '/PasswordAuthentication/d' /etc/ssh/sshd_config
sed -i '/PermitRootLogin/d' /etc/ssh/sshd_config

echo ''
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config

